import multilayer as ml
import numpy as np
import math
import time


#Import in data
k = 5 	#Needs to be at least 2
num_data = float(10000*k)/float(k-1)
num_data = int(k*math.ceil(num_data/float(k)))


data_file = file("../data/full.dat","r")
info = []
ptz = []

i = 0
for line in data_file:
	i += 1
	num_line = line.split()
	if i <= num_data:
		info.append([float(x) for x in num_line[:-1]])
		ptz.append(float(num_line[-1]))
	else:
		break

info = np.asarray(info,dtype=np.float32)
ptz = np.asarray(ptz,dtype=np.float32)

data_file.close()
	
#Shuffle data
perm = np.arange(num_data)
np.random.shuffle(perm)
info = info[perm]
ptz = ptz[perm]

info_sets = np.split(info,k)
ptz_sets = np.split(ptz,k)



cost_list = []
#time_dat = open('time.dat','w')
#k-fold cross validation for 1 hidden layer
for r in range(1,11):
	for i in range(1,r+1):
		cost = 0.0
		for j in range(k):
			train_info_set = info_sets
			train_ptz_set = ptz_sets
			valid_info_set = train_info_set[j]
			valid_ptz_set = train_ptz_set[j]
			train_info_set = np.concatenate(np.delete(train_info_set,j,0),axis=0)
			train_ptz_set = np.concatenate(np.delete(train_ptz_set,j,0),axis=0)
			new_cost = ml.run_nn(True,r,i,train_info_set,train_ptz_set,valid_info_set,valid_ptz_set)
			cost += new_cost
			#curr_time = time.gmtime()
			#time_dat.write("Nodes: %d, Fold: %d, Time: %d/%d %d:%d:%d\n" % (i,j,curr_time[1],curr_time[2],curr_time[3],curr_time[4],curr_time[5])
		cost = cost/float(k)
		cost_list.append(cost)
#time_dat.close()
np.savetxt('../data/k_fold_1.dat',cost_list)
