#This is absolutely the worst way to convert the data into a table format
import math

dat = open('k_fold_2.dat','r')

table = []

for line in dat:
	table.append(math.log(float(line)))
dat.close()

dat_table = []
for i in range(1,11):
	for j in range(0,11):
		dat_table.append([i,j])


for i in range(1,11):
	for j in range(1,i+1):
		val = table.pop(0)
		ind = dat_table.index([i,j])
		dat_table[ind].append(val)


one_dat = open("k_one.dat","r")
one_table = []
for line in one_dat:
	nums = line.split()
	one_table.append(math.log(float(nums[1])))
one_dat.close()

for i in range(0,10):
	val = one_table.pop(0)
	dat_table[i*11].append(val)


for i in range(0,110):
	if len(dat_table[i]) == 2:
		dat_table[i].append(10.0)
new_dat = open('k_200.dat','w')
for i in range(0,110):
	new_dat.write("%d %d %f\n" % (dat_table[i][0],dat_table[i][1],dat_table[i][2]))
new_dat.close()
