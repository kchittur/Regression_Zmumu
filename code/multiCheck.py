import tensorflow as tf
import data_func as df
import numpy as np


n_hidden1 = 6 
#n_hidden2 = 6 
n_input = 6 
n_out = 1

x = tf.placeholder("float", [None, n_input])
y = tf.placeholder("float", [None, n_out])

def model(x, weights, biases):
        hidden_layer1 = tf.matmul(x, weights['h1']) +  biases['b1']
        hidden_layer1 = tf.nn.tanh(hidden_layer1)

        #hidden_layer2 = tf.matmul(hidden_layer1,weights['h2']) + biases['b2']
        #hidden_layer2 = tf.nn.tanh(hidden_layer2)

        out_layer = tf.matmul(hidden_layer1, weights['out']) + biases['out']
        return out_layer

weights = {
        'h1' : tf.Variable(tf.random_normal([n_input,n_hidden1])),
        #'h2' : tf.Variable(tf.random_normal([n_hidden1,n_hidden2])),
        'out' : tf.Variable(tf.random_normal([n_hidden1,n_out]))
}

biases = {
        'b1' : tf.Variable(tf.random_normal([n_hidden1])),
        #'b2' : tf.Variable(tf.random_normal([n_hidden2])),
        'out' : tf.Variable(tf.random_normal([n_out]))
}


pred = model(x,weights,biases)


test_info_norm,test_info, test_ptz, info_std, info_mu, ptz_std, ptz_mu = df.load_data(True)

saver = tf.train.Saver()

with tf.Session() as sess:

	saver.restore(sess,"weights/model_288_adadelta.ckpt")
	
	correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
	print "Accuracy:", accuracy.eval({x: test_info, y: test_labels})
	


	accuracy = tf.reduce_sum(tf.square(pred-
