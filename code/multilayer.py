import tensorflow as tf
import numpy as np


def next_batch(batch_size,info,labels):
	dim = labels.size
        perm = np.arange(dim)
        np.random.shuffle(perm)
        info = info[perm]
        labels = labels[perm]
        labels = np.reshape(labels,(dim,1))
        return info[0:batch_size], labels[0:batch_size]


def model(x, weights, biases):
	hidden_layer1 = tf.matmul(x, weights['h1']) +  biases['b1']
	hidden_layer1 = tf.nn.relu(hidden_layer1)
	hidden_layer2 = tf.matmul(hidden_layer1, weights['h2']) + biases['b2']
	hidden_layer2 = tf.nn.relu(hidden_layer2)
	out_layer = tf.matmul(hidden_layer2, weights['out']) + biases['out']
	return out_layer

def run_nn(two_layers,n_hidden1,n_hidden2,info,labels,valid_info,valid_labels):

	n_input = 6 
	n_out = 1
	

	weights = {
		'h1' : tf.Variable(tf.random_normal([n_input,n_hidden1])),
		'h2' : tf.Variable(tf.random_normal([n_hidden1,n_hidden2])),
		'out' : tf.Variable(tf.random_normal([n_hidden2,n_out]))
	}

	biases = {
        	'b1' : tf.Variable(tf.random_normal([n_hidden1])),
		'b2' : tf.Variable(tf.random_normal([n_hidden2])),
        	'out' : tf.Variable(tf.random_normal([n_out]))
	}


	learning_rate = 0.001
	batch_size = 10000
	training_epochs = 200


	x = tf.placeholder("float", [None, n_input])
	y = tf.placeholder("float", [None, n_out])

	pred = model(x,weights,biases)

	cost = tf.reduce_mean(tf.square(pred-y))
	optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)


	init = tf.global_variables_initializer()

	#saver = tf.train.Saver()

	with tf.Session() as sess:
		print "Starting Optimization"
		sess.run(init)
		for epoch in range(training_epochs):
			avg_cost = 0.0
			for i in range(1000):
				batch_xs, batch_ys = next_batch(batch_size,info,labels)
				_, c = sess.run([optimizer,cost], feed_dict={x: batch_xs, y: batch_ys})
				avg_cost += c / float(batch_size) 
				if(i%100==0):
					print "Finished batch",'%05d' % i, " of 1000"
			print "Epoch:", '%04d' % (epoch+1), "cost =", "{:.9f}".format(avg_cost), "nodes =", '%d %d ' % (n_hidden1,n_hidden2)
			#save_path = saver.save(sess, "../weights/model_288_adadelta.ckpt")
			
		print "Optimization Complete!"

		valid_pred = pred.eval({x:valid_info})

		valid_pred = valid_pred.reshape(2500,1)
		valid_labels = valid_labels.reshape(2500,1)
	
		new_tens = np.concatenate((valid_labels,valid_pred),axis=1)
		print(new_tens)

                return np.mean(np.square(valid_pred-valid_labels))
