import numpy as np



def load_data(test = False):
	data_file = file("../data/full.dat","r")
	#next(data_file)
	info = []
	test_info= []

	ptz = []
	test_ptz = []

	i = 0
	for line in data_file:
		i += 1
		num_line = line.split()
		if i <= 70000:
			info.append([float(x) for x in num_line[:-1]])
			ptz.append(float(num_line[-1]))
		elif i <= 100000 and test == True:
			test_info.append([float(x) for x in num_line[:-1]])
			test_ptz.append(float(num_line[-1]))
		else:
			break
	

	info = np.asarray(info,dtype=np.float32)
	test_info = np.asarray(test_info, dtype=np.float32)

	ptz = np.asarray(ptz,dtype=np.float32)
	test_ptz = np.asarray(test_ptz,dtype=np.float32)

	data_file.close()



	
	'''
	
	info_std = np.std(info, axis = 0, dtype=np.float64)
	info_mu = np.mean(info, axis = 0, dtype=np.float64)	
	#info = (info-info_mu)/info_std

	ptz_std = np.std(ptz,axis = 0, dtype=np.float64)
	ptz_mu = np.mean(ptz, axis = 0, dtype=np.float64)
	#ptz = (ptz-ptz_mu)/ptz_std
	'''	

	if test == True:
		test_info_norm = test_info	
		#test_info_norm = (test_info-info_mu)/info_std
		perm = np.arange(30000)
		np.random.shuffle(perm)
		test_info_norm = test_info_norm[perm]
		test_ptz = test_ptz[perm]
		return test_info_norm, test_ptz
		#return test_info, test_ptz
	else:
		return info, ptz 


def next_batch(batch_size,info,labels):
	perm = np.arange(70000)
	np.random.shuffle(perm)
	info = info[perm]
	labels = labels[perm]
	labels = np.reshape(labels,(70000,1))
	return info[0:batch_size], labels[0:batch_size]

