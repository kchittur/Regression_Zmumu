import math

with open("../data/361107pt.dat") as f:
	next(f)
	data = []
	for line in f:
		w = [float(x) for x in line.split()]
		data.append(w)


f = open("../data/full.dat","w")

#0   1    2   3   4   5   6
#px0 px1 py0 py1 pz0 pz1 ptz
for line in data:
	px = math.pow(line[0]+line[1],2)
	py = math.pow(line[2]+line[3],2)
	ptz = math.pow(line[6],2)
	
	f.write(" %.7f %.7f %.7f %.7f %.7f %.7f %.7f\n" % (line[0],line[1],line[2],line[3],line[4],line[5],line[6]))


f.close()
