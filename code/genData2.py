import tensorflow as tf
import data_func as df
import numpy as np
from tensorflow.python.framework import ops

n_hidden1 = 5
n_input = 6
n_out = 1

x = tf.placeholder("float", [None, n_input])
y = tf.placeholder("float", [None, n_out])


# relu vs softmax vs tanh
def model(x, weights, biases):
        hidden_layer1 = tf.matmul(x, weights['h1']) +  biases['b1']
        hidden_layer1 = tf.nn.relu(hidden_layer1)
        
	out_layer = tf.matmul(hidden_layer1, weights['out']) + biases['out']
        return out_layer

weights = {
        'h1' : tf.Variable(tf.random_normal([n_input,n_hidden1])),
        'out' : tf.Variable(tf.random_normal([n_hidden1,n_out]))
}

biases = {
        'b1' : tf.Variable(tf.random_normal([n_hidden1])),
        'out' : tf.Variable(tf.random_normal([n_out]))
}



pred = model(x,weights,biases)

test_info_norm, test_labels = df.load_data(True)


test_data = []

test_data= np.asarray(test_info_norm, dtype=np.float32)


saver = tf.train.Saver()


print(test_data)

with tf.Session() as sess:

	saver.restore(sess,"../weights/model_288_adadelta.ckpt")
	pred = pred.eval({x:test_data})


#pred *= ptz_std
#pred += ptz_mu

print(pred)

#test_data *= info_std
#test_data += info_mu

test_data = np.column_stack((test_data,pred))



np.savetxt("../data/data.dat",test_data, fmt='%f %f %f %f %f %f %f',delimiter=' ')
print("Generated Data!")

