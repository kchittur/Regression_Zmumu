import math

f = open("../data/data.dat","r")
g = open("../data/diff.dat","w")
g.write("Est. Act.\n")
tot_sum = 0.0
i = 0
mean = 0.0
for line in f:
	num_line = line.split()
	num_line = [float(x) for x in num_line]
	ptx = math.pow(num_line[0]+num_line[1],2)
	pty = math.pow(num_line[2]+num_line[3],2)
	ptz = math.sqrt(ptx+pty)
	g.write("%f %f\n" % (ptz,num_line[-1]))
	
	i+=1
	tot_sum += math.pow(ptz-num_line[-1],2)
	mean += num_line[-1]

f.close()
g.close()
